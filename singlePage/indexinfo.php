<!DOCTYPE html>
<html>
<head>
<title>Welcome!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to elasicActs!</h1>

    <?php echo '<p>Hello,</p>';

    // Define PHP variables for the MySQL connection.
    $servername = "localhost";
    $username = "root";
    $password = "myDb123";

    // Create a MySQL connection.
    $conn = mysqli_connect($servername, $username, $password);

    // Report if the connection fails or is successful.
    if (!$conn) {
        exit('<p>Your connection has failed.<p>' .  mysqli_connect_error());
    }
    echo '<p>You have connected successfully.</p>';

if ( function_exists( 'mail' ) )
{
    echo 'mail() is available';
}
else
{
    echo 'mail() has been disabled';
}


    /*
     * Enable error reporting
     */
    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );

    /*
     * Setup email addresses and change it to your own
     */
    $to = "elasticActs <elasticacts@gmail.com>";
    $subject = "Simple test for mail function";
    $message = "This is a test to check if php mail function sends out the email";

    // $headers = "From:" . $from;
    // $headers .= "Reply-To: kenwoo@yourdomain.com";

    // To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'To: Kenwoo <kenu717@gmail.com>';
$headers[] = 'From: Birthday Reminder <birthday@example.com>';
// $headers[] = 'Cc: birthdayarchive@example.com';
$headers[] = 'Bcc: avantians@gmail.com';

    /*
     * Test php mail function to see if it returns "true" or "false"
     * Remember that if mail returns true does not guarantee
     * that you will also receive the email
     */
    if(mail($to, $subject, $message, implode("\r\n", $headers)))
    {
        echo "<br/>Test email send.";
    }
    else
    {
        echo "<br/>Failed to send.";
    }
?>

</body>
</html>
