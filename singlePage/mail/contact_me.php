<?php
// Check for empty fields
if(empty($_POST['name'])      ||
   empty($_POST['email'])     ||
   empty($_POST['phone'])     ||
   empty($_POST['message'])   ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
   echo "No arguments Provided!";
   return false;
   }

$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));
$date = date("F j, Y, g:i a");

// Create the email and send the message
$to = 'Channy Park<chanhun71@gmail.com>';
$email_subject = "Website Contact Form:  $name";
$email_body = "This message is from the website contact form.<br/>"."Here are the details:<br/><br/><strong>Date:</strong> $date<br/><strong>Name:</strong> $name<br/><strong>Email:</strong> $email_address<br/><strong>Phone:</strong> $phone<br/><strong>Message:</strong><br/><br/>$message";

$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';
// Additional headers
$headers[] = 'From: '. $name .' <'. $email_address .'>';
// $headers[] = 'Cc: birthdayarchive@example.com';
$headers[] = 'Bcc: Avantians <okchurchweb@gmail.com>';
// mail($to, $email_subject, $email_body, $headers);
mail($to, $email_subject, $email_body, implode("\r\n", $headers));
return true;
?>
