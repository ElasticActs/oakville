<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="옥빌 한인 교회 | Oakville Korean Church">
    <meta name="author" content="elasticActs">

        <meta property="og:title" content="Oakville Korean Church/">
        <meta property="og:type" content="article">
        <meta property="og:url" content="http://oakvillekoreanchurch.com/">
        <meta property="og:description" content="Oakville Korean Church">
        <meta property="og:site_name" content="oakvillekoreanchurch.com">

    <title>옥빌 한인 교회 | oakvillekoreanchurch.com</title>

    <!-- Bootstrap core CSS -->
    <link href="singlePage/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="singlePage/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="singlePage/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="singlePage/css/creative.css" rel="stylesheet">
    <link rel="shortcut icon" href="singlePage/favicon.ico">
  </head>

  <body id="page-top">
    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <img src="singlePage/img/oakville_korean_church.png" alt="옥빌 한인 교회">
              <!--strong>옥빌 한인 교회</strong-->
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-center mb-5">옥빌 한인 교회 홈페이지를 찾아 주셔서 감사합니다. <br/>홈페이지가 업데이트 진행 중에 있습니다. 곧 새로운 모습으로 찾아 뵙겠습니다.</p>
            <p class="text-left" style="padding-left: 105px;">
            <strong>예배: 매 주일 오후 1시</strong><br/>
            주소: 5 Dundas St. E. OAKVILLE, ON L6H 7C4 CA<br/>
            <p class="text-right">
            chanhun71@gmail.com / 647.523.6127<br/>
            박찬훈 목사외 교우 일동</p>
            </p>
          </div>
        </div>
      </div>
    </header>

   <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Let's Get In Touch!</h2>
            <hr class="my-4">
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate="novalidate">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

        <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <span class="copyright">Copyright &copy; <?php echo date("Y"); ?> 옥빌 한인 교회 | Oakville Korean Church</span>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="singlePage/vendor/jquery/jquery.min.js"></script>
    <script src="singlePage/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="singlePage/js/jqBootstrapValidation.js"></script>
    <script src="singlePage/js/contact_me.js"></script>

    <!-- Plugin JavaScript -->
    <script src="singlePage/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="singlePage/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="singlePage/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="singlePage/js/creative.min.js"></script>

  </body>

</html>
