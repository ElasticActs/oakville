<?php
$GeneralItems = new getGeneral( $base_url );
//echo $GeneralItems->getFrontItems( "articles", 12, "imgboard#4", "#4#4OR5OR7" );
?>
<div class="front-left">
    <ul class="castingthumblist">
        <li class="list_1">
            <?php echo $GeneralItems->getFrontItems( "articles", 1, "castpic", "mostrecent#5#4" ); ?>
        </li>
    </ul>
</div>

<div class="front-right">
    <ul class="column-list">
        <li>
            <a href="<?php echo $base_url; ?>/sermons/all-sermons" target="_self" title="목회 묵상">말씀의 나눔</a>
        </li>
        <?php echo $GeneralItems->getFrontItems( "articles", 5, "articles", "#5#4"); ?>
    </ul>
</div>
</div>
</section>

<hr class="black-line">

<div class="text-center">
    <p class="blue txtszie korea-txt">
        <span class="green">The WORD</span> centered & <span class="red">Missional Church</span> centered<br/>
    </p>
    <span class="neonblue-txt korea-txt"><strong>성경 중심의 말씀 사역과 섬기며 나누는 공동체</strong></span>
</div>

<hr class="black-line">

<?php
// echo "<section class=\"content\">"
//         ."<div class=\"arrow-down-white general-shattered-fixed-banner\">"
// 		."<div class=\"container-for-arrow\">";
// echo $GeneralItems->getFrontItems( "articles", 6, "front_imgboard", "#6#9" );
// echo "</div></div></section>";

// echo $GeneralItems->getFrontItems( "articles", 1, "articles", "#4#10");
?>

    <section class="content">
		<div class="container-with-padding">
					<div class="front-fixed-one">
						<ul class="column-list">
                            <li>
                                <a href="<?php echo $base_url; ?>/sharing/info-board" target="_self" title="교회 소식">교회 소식</a>
                            </li>
                            <?php echo $GeneralItems->getFrontItems( "articles", 6, "articles", "#4#10"); ?>
						</ul>
					</div>
					<div class="front-fixed-two">
                        <ul class="column-list">
                            <li>
                                <a href="<?php //echo $base_url; ?>/sermons/pastoral-column" target="_self" title="목회 묵상">목회 묵상</a>
                            </li>
                            <?php echo $GeneralItems->getFrontItems( "articles", 6, "articles", "#4#5"); ?>
                        </ul>
					</div>
					<div class="front-fixed-three">
                        <ul class="column-list">
                            <li>
                                <a href="<?php echo $base_url; ?>/about-oakville-korean-church" target="_self" title="교회 소개">교회 소개</a>
                            </li>
                            <li>
                               <a href="http://www.oakvillekoreanchurch.com/about-oakville-korean-church" title="옥빌 한인교회 소개" target="_self" style="display:table;">
                <img src="http://www.oakvillekoreanchurch.com/upload/images/logo-explain-small.png"></a>
                            </li> 
                            <li>
                                <a href="<?php echo $base_url; ?>/about-oakville-korean-church/greeting" target="_self" title="교회 인사">교회 인사</a>
                            </li>                                                        
                        </ul>
					</div>
		</div>
	</section>