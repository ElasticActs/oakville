<?php
/** -------------------------------------------------------------------------
 * This program is Open Source; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * This program is coded in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY
 * @package  CMS
 * @author     Kenwoo - iweb@kenwoo.ca
 * @license    http://creativecommons.org/licenses/by/4.0/ Creative Commons
 *
 * [v01-09/01/2015]:: Set flag, not allow to direct access
 *  ------------------------------------------------------------------------- */
defined( "_VALID_MOS" ) or die( "Your system is not working properly." );
?>
<!--
<div class="foot-right">
<a href="https://www.facebook.com/groups/tconnectchurch/" title="Facebook" target="_blank" class="facebook_icon"></a> <span class="logo_icon"></span>
<a href="https://www.youtube.com/channel/UCpZQ8_EhcKAiheE-gfaADrA" title="YouTube" target="_blank" class="youtube_icon"></a><br class="clearfix">
</div>
-->
Copyright &copy; 2019 - 옥빌 한인 교회 | Oakville Korean Church. All Rights Reserved.<br/>
교회:<a href="https://goo.gl/maps/gpBpFvm97agU1Fb17" title="View Google Map" target="_blank"> 5 Dundas St. E. OAKVILLE, ON <span class="disapearclass">L6H 7C4</span></a><br/>
647.523.6127 / okchurchweb@gmail.com
